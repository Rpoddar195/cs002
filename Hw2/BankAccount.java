/* Ritika Poddar
 * CSE 017
 * RIP218
 * Homework #2  Due: 2/7/16
 * Program Name: Bank Accounts */

public class BankAccount{ //this class contains bank accounts as objects 

  // data fields
  int id;
  String customer;
  double balance;
  double annualInterestRate;
  
  //methods:
  // this a thre arg constructor for an account given an  
  // id, cutsomer name, and annual interest rate
  
  public BankAccount  (int aId, String aCustomer, double theInterestRate){
    id = aId;
    customer = aCustomer;
    annualInterestRate = theInterestRate;
    
  }
  //this is a 4 arg constructor
  public BankAccount (int aId, String aCustomer, double aBalance, double theInterestRate){
    id = aId;
    customer = aCustomer;
    annualInterestRate = theInterestRate;
    balance = aBalance;
  }
  
  //method for finding the monthly interest rate based on annual rate
  public double getMonthlyInterestRate ( ){
  return (Math.pow((1.0 + annualInterestRate),(.8333))) - 1.0;
   
  }
    
  
  public void withdraw ( double anAmount){
    balance -= anAmount; //subtracts the withdrawal amount from the balance
    
  }
  
   public void deposit ( double anAmount ){
   balance += anAmount; //adds the deposit amount from the balance
  }
  
   public void accrueMonthlyInterest (){ 
   double monthlyinterest = (balance*getMonthlyInterestRate()) ;
   balance += monthlyinterest;
   
 } 
   // method to print the infomation about each account
   public void printAccountInfo (){
     System.out.println(" Account: " + id );
     System.out.println(" Customer: " + customer);
     System.out.println(" Annual Interest Rate: " + annualInterestRate * 100 + "%");
     System.out.println(" Balance: $" + balance);
   }   
   
   public static void main (String [] args){
     
     BankAccount myAccount = new BankAccount (1001, "Peter Parker", .0075); //creates first object for the bank account
     
     BankAccount secondAccount = new BankAccount (1002, "Tony Stark", 100000.00, .015); // creates second object for bank account
 
   System.out.println("Before Transactions");  
  //calls method to print account infomation based on object fields that were entered above
   myAccount.printAccountInfo();
   secondAccount.printAccountInfo();
                                                    
    // calls deposit and withdraw methods
    myAccount.deposit(49.99);
    secondAccount.withdraw(27499.99);
    
    //calls the monthly interest method to accrue the monthly interest on account balance
    myAccount.accrueMonthlyInterest();
    secondAccount.accrueMonthlyInterest(); 
  
    System.out.println("After transactions have finished");
    
    //calls method to print account infomation
     myAccount.printAccountInfo();
    
     secondAccount.printAccountInfo();
  
   
   }



}