/* CSE 17
 * Ritika Poddar
 * rip218
 * Homework #3
 * Due: March 1, 2016
 * Program: Text Messaging
 */

import java.util.Scanner;
import java.io.File;
import java.io.PrintWriter;

class TextMessage{
  private String recipient;
  private String keyPresses;
  private String finalDecoded;
  
 // constructor 
  public TextMessage (String recipient, String keyPresses){
    this.recipient = recipient; 
    this.keyPresses = keyPresses; 
    
  }
  
  public String getkeyPresses(){
    return keyPresses;
  }
  
  public String getRecipient(){
    return recipient;
  }
  
  public String getfinalDecoded(){
    return finalDecoded;
    
  }
  
  /*Initializes the messages array to be able to hold
up to 10 messages and sets the msgCount
to 0 */
  public String getDecodedMessage(){
    String keyPresses = getkeyPresses();
    String c = Character.toString(keyPresses.charAt(0));
   int i;
    for(i =1; i <keyPresses.length(); i++){
      if(keyPresses.charAt(i) != ' '){
        if(keyPresses.charAt(i) == keyPresses.charAt(i-1)){
          c = c + Character.toString(keyPresses.charAt(i));
        }
        
        else {
          decoding(c);
          c = Character.toString(keyPresses.charAt(i)); // counter back to current position
        }
      }
    }
   // at end of the message
    if ( i == keyPresses.length()-1){
      decoding(c);
    }
    return finalDecoded;
  }
  
      // decoding    
      public void decoding(String c){
      
        switch (c.charAt(0)){
        
        case ' ':
          finalDecoded += "0";
          break;
         
        case '1':
          finalDecoded += ".";
          break;
        
        case '2':
          if ( c.length() == 1) {
          finalDecoded = finalDecoded + 'A' ;
        }
          else if (c.length() == 2){
            finalDecoded = finalDecoded + 'B' ;
          }
          else if (c.length() == 3){
            finalDecoded = finalDecoded + 'C' ;
          }
          break;
          
        case '3':
          if (c.length() == 1) {
          finalDecoded = finalDecoded + 'D';
        }
          else if (c.length() == 2){
           finalDecoded = finalDecoded + 'E';
          }
          else if (c.length() == 3){
           finalDecoded = finalDecoded + 'F';
          }
          break;
     
        case '4':
          if (c.length() == 1) {
          finalDecoded = finalDecoded +'G';
        }
          else if (c.length() == 2){
           finalDecoded = finalDecoded + 'H';
          }
          else if (c.length()== 3){
            finalDecoded = finalDecoded + 'I';
          }
          break;
     
        case '5':
          if (c.length() == 1) {
          finalDecoded = finalDecoded + 'J';
        }
          else if (c.length() == 2){
            finalDecoded = finalDecoded + 'K';
          }
          else if (c.length() == 3){
            finalDecoded = finalDecoded + 'L';
          }
          break;
          
           case '6':
          if (c.length()== 1) {
          finalDecoded = finalDecoded +'M';
        }
          else if (c.length() == 2){
           finalDecoded = finalDecoded + 'N';
          }
          else if (c.length() == 3){
            finalDecoded = finalDecoded +'O';
          }
          break;
          
           case '7':
          if (c.length() == 1) {
          finalDecoded = finalDecoded + 'P';
        }
          else if (c.length()== 2){
            finalDecoded  = finalDecoded + 'Q';
          }
          else if (c.length() == 3){
            finalDecoded  =  finalDecoded + 'R';
          }
          else if (c.length() == 4){
            finalDecoded  = finalDecoded + 'S';
          }
          break;
           
         case '8':
          if (c.length() == 1) {
        finalDecoded= finalDecoded + 'T';
        }
          else if (c.length()== 2){
           finalDecoded  = finalDecoded + 'U';
          }
          else if (c.length() == 3){
           finalDecoded = finalDecoded + 'V';
          }
          break;
          
           case '9':
          if (c.length() == 1) {
          finalDecoded = finalDecoded+  'W';
        }
          else if (c.length() == 2){
            finalDecoded = finalDecoded + 'X';
          }
          else if (c.length() == 3){
            finalDecoded = finalDecoded + 'Y';
          }
           else if (c.length() == 4){
            finalDecoded = finalDecoded + 'Z';
          }
          break;
          
      }
  
    
  }
  
}



public class TextDecoder {
  private TextMessage [] messages;
  private int msgCount;
    
  
    
    
   public void TextDecoder(){
    TextMessage[] messages = new TextMessage[10];
    msgCount = 0;
  }
  
   /*Reads messages from the file specified by the
msgFile object, updating messages and
msgCount appropriately. */
  public void readMessagesFromFile(File msgFile) throws Exception {
    
    Scanner code = new Scanner (msgFile);
    while (code.hasNext()){
    String msg = code.nextLine();
    String string = new String(msg);
    String phoneNumb = string.substring (0, 10);
    String msgText = string.substring(11);
    TextMessage mymsg = new TextMessage (phoneNumb, msgText);
    messages[msgCount] = new TextMessage (mymsg.getRecipient(), mymsg.getDecodedMessage());
    msgCount++;
    
    }
 
   code.close();
  }
  
  /* Prints all of the messages using the form:
“recipient:[tab]decoded-message” */
  public void printMessages(){
    for(int i=0; i< msgCount; i++){
      System.out.println( messages[i]);
    
  }
  }
  
 /*Checks if there is one command-line argument
and exits with a helpful error message if
not. Otherwise, reads a set of messages
from the specified file and then prints out
the decoded messages in the form above. */
  
  public static void main (String[] args) throws Exception{
    
    if (args.length != 1){
      System.out.println("Error Message");
    System.exit(1);
    }
    else{
     File msgFile = new File (args[0]);
     TextDecoder decode = new TextDecoder();
     
      decode.readMessagesFromFile(msgFile);
      decode.printMessages();
    }
  }
  } 