
/*
 * CSE017
 * Ritika Poddar
 * RIP218
 * HW#4 Due: March 26, 2016
 * Program description: AlbumList
 */

/** A collection of musical recordings.*/
public class Album{
  private String title;
  private String artist;
  private int year;
  private int rank;
  
 /**Initialize fields using parameter values.*/ 
  public Album(String title, String artist, int year, int rank){
   this.title = title;
   this.artist = artist;
   this.year = year;
   this.rank = rank;
   
  }
  /** Initialize fields using parameter values. Sets rank to -1.*/
   public Album(String title, String artist, int year){
   this.title = title;
   this.artist = artist;
   this.year = year;
   this.rank = -1;
   
  }
   
   /** getter*/
   public String getTitle(){
     return title;
   }
   
    /** getter*/
   public String getArtist(){
     return artist;
   }
   
    /** getter*/
   public int getYear(){
     return year;
   }
   
    /** getter*/
   public int getRank(){
     return rank;
   }
   
   /**setter*/
   public void setRank (int rank){
     this.rank = rank;
   }
   
   /** Returns true if obj has same title and artist as current object*/
  public boolean equals(Object obj){
    if( obj instanceof Album){
      Album temp = (Album) obj;
      if(this.artist.equals(temp.artist) && this.title.equals( temp.title)){
        
        return true;
      }
    }
    return false;
    
    
  }
   

   /**Returns a string "rank title artist year" sep by tabs and oranized in specific columns */
  public String toString(){
    
    String s = String.format("%-4s \t %-30s \t %-20s \t %-4s ", rank, title, artist, year);
    return s; 
   }
   
}