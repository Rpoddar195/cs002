/*
 * CSE017
 * Ritika Poddar
 * RIP218
 * HW#4 Due: March 26, 2016
 * Program description: AlbumList
 */


import java.util.ArrayList;
import java.io.File;
import java.util.Scanner;
import java.io.*;
import java.util.*;

/**class that represents a list of albums */
public class AlbumList{
  private ArrayList<Album> albums;
  
  /**initializes album to be an empty list */
  public AlbumList(){
    albums = new ArrayList<Album>();
  }

  /* reads albums from file */
  public void readAlbumsFromFile(File inFile){  
    int counter = 0;  
    int rank =1;
    try{
      Scanner code = new Scanner(inFile);
      code.useDelimiter("\\t|[\\n\\r\\f]+");
    
     while(code.hasNext()){
       
       try{
        counter++;
        String albumArtist = code.next();
       // System.out.println(albumArtist);
        String albumTitle = code.next();
       // System.out.println(albumTitle);
        int albumYear = code.nextInt();
       // System.out.println(albumYear);
       
        try{
        Album newAlbum = new Album(albumTitle, albumArtist, albumYear, rank);
        addAlbum(newAlbum);
        rank++;
        }
         catch(DuplicateAlbumException ex){
         // System.out.println("ERROR: line: "  + counter + " Duplicate Album '" + albumTitle + "' by " + albumArtist);
         } 
      
       }
       catch(InputMismatchException ex){
       System.out.println("ERROR: line " + counter + ": Invalid input for year. Skipping line");
       code.next();
       }
      

      }//end while loop
    }//end try block
   
    catch(java.io.FileNotFoundException ex){
      System.out.println("ERROR: File not found");
    } 

    
        
  }// end of readalbumfromfile
  
  
  /** Adds an album to the end of albums. If duplicate, throws a DuplicateAlbumException without adding the album.*/
  public void addAlbum(Album album) throws DuplicateAlbumException{
    for(int i = 0; i<albums.size(); i++){
    
      if((albums.get(i)).equals(album)){
        throw new DuplicateAlbumException(album.getTitle(), album.getArtist());
      }
     
    }
    albums.add(album);// if not duplicate add to list
    
  }
  
  
   /**prints out the albums using toString*/
  public void printAlbums() {
     
    System.out.println("Album Rankings from top20albums.txt");
    System.out.println("Rank       Title                                    Artist                         Year");
    System.out.println("----       ----------------------------             -------------------            ----");
   
    for(int i=0; i<albums.size();i++){
      System.out.println(albums.get(i).toString());
    }
  }

  /**reads file and  makes sure user enters in a file, then calls the other methods to start creating list*/
  public static void main(String[] args) throws Exception{
  
    if (args.length != 1 ) {
      System.out.println("Error");
    System.exit(1);
    }
    
    else{ 
    File inFile = new File(args[0]);
  
    AlbumList read = new AlbumList();
    read.readAlbumsFromFile( inFile);
    read.printAlbums();
    }
  }
  



}// end of class

   