/*
 * CSE017
 * Ritika Poddar
 * RIP218
 * HW#4 Due: March 26, 2016
 * Program description: AlbumList
 */

/**extends ArrayStoreException. Signals that a duplicate album was added to an album list. */
public class DuplicateAlbumException extends ArrayStoreException{
  //indicates that a duplicate album was added to an album list
  
  public DuplicateAlbumException(String title, String artist){
    System.out.println("ERROR: Dublicate album '" + title + "' by " + artist);
  }
}