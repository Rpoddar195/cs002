/*
 * CSE017
 * Ritika Poddar
 * RIP218
 * Homework #5 Due: April 23, 2016
 * Tutor: Wellesley Arreza <wra216@lehigh.edu>
 * Program description: Comedy
 */
import java.util.ArrayList;
import java.io.File;
import java.util.Scanner;
import java.io.*;
import java.util.*;
import java.util.Date;
import java.text.SimpleDateFormat;

/**musical object */
public class Musical extends Show{
  
  protected String composer;
  
  /** creates new Musical*/
  public Musical (String name, String playwright, String composer, String theater,Date openDate){
    super(name, playwright, theater, openDate);
    this.composer = composer;
    
  }
  
  /** getter*/
  public String getComposer(){
    return composer;
    
  }
  
  /** Creates a string that corresponds to the output required */
  public String toString(){
    SimpleDateFormat s = new SimpleDateFormat("MM/dd/yy");
    String date = s.format(openDate);
  
    return name + " - Playwright: " + playwright + ", Composer: " + composer + " at " + theater
  + " since " + date;
    
  }
  
  
  
  
}// end of class