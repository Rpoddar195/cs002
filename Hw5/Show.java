/*
 * CSE017
 * Ritika Poddar
 * RIP218
 * Homework #5 Due: April 23, 2016
 * Tutor: Wellesley Arreza <wra216@lehigh.edu>
 * Program description: Comedy
 */
import java.util.ArrayList;
import java.io.File;
import java.util.Scanner;
import java.io.*;
import java.util.*;
import java.util.Date;
import java.text.SimpleDateFormat;

/**  show object*/
public class Show{
  
  protected String name; // name of show
  protected String playwright;
  protected String theater;
  protected Date openDate; // day of first show
  
  /** creates a new show*/
  public Show(String name, String playwright, String theater, Date openDate){
  this.name = name;
  this.playwright = playwright;
  this.theater = theater;
  
  this.openDate = openDate;
  
  }
  
  /** Creates a string that corresponds to the output required */
  public String toString(){
    SimpleDateFormat s = new SimpleDateFormat("MM/dd/yy");
    String date = s.format(openDate);
    return name + " - Playwright: " + playwright  + " at " + theater
  + " since " + date;
    
  }
  
  /** getters*/
  public String getName(){
    return name;
    
  }
    
   public String getPlaywright(){
    return playwright;
    
  }
    
    public String getTheater(){
    return theater;
    
  }
    
     public Date getOpenDate(){
     
       
       return openDate;
    
  }
    
    
  
  
  
  
  
  
  
  
  
}