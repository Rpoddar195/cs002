/*
 * CSE017
 * Ritika Poddar
 * RIP218
 * Homework #5 Due: April 23, 2016
 * Tutor: Wellesley Arreza <wra216@lehigh.edu>
 * Program description: Comedy
 */
import java.util.ArrayList;
import java.io.File;
import java.util.Scanner;
import java.io.*;
import java.util.*;
import java.util.Date;

/** object for list of shows */
public class TheaterSchedule  <E>{
  
  ArrayList <E> allShows;
  
  /**constructor*/
  public TheaterSchedule() {
    allShows = new ArrayList <E> ();
    
   }

  /** adds shows to array */
  public void addShow(E newShow){
    allShows.add(newShow);
    
  }
  
  /** gets show at specific index */
  public E getShowAt(int index){
    return allShows.get(index);
    
  }
  
  /** returns number of shows */
  public int getNumShows(){
    return allShows.size();
    
  }
  
  /** prints each show out according each object type*/
  public static void printSchedule(TheaterSchedule list){
    for(int i =0; i< list.allShows. size(); i++){
      System.out.println(list.getShowAt(i));
      
    }
    
  }
  
  /**returns an array of all shows that have been open since that date*/
  public static  <E extends Show> E[] getShowsOpenSince(TheaterSchedule <E> shows, Date x){
  int y = shows.getNumShows();
  int index = 0;
  int counter = 0;
  
    E [] list = (E[]) new Show [y];
  
    /** find how many shows are relevant in order to know length of array*/
    for(int i = 0; i< y; i++){
      if(((shows.getShowAt(i).getOpenDate()).equals (x))  ||  ((shows.getShowAt(i).getOpenDate()).before (x)) ){
      index ++;
      
      }
      
    } 
    
    for(int i =0; i< y; i++){
      if(((shows.getShowAt(i).getOpenDate()).equals (x))  ||  ((shows.getShowAt(i).getOpenDate()).before (x)) ){
      list[counter] = shows.getShowAt(i);
      counter ++;
      
      
      }
    }
    
     E [] newlist = (E[]) new Show [index];
    /** fix array to get rid of null values */ 
    for (int i = 0; i< index; i ++){
      newlist[i] = list[i];
      
    }
 
    return newlist;
  }
  
  
  /** prints out composers with their shows */
  public static <E extends Musical > void printComposers(TheaterSchedule <E> shows){
    for(int i = 0; i< shows.getNumShows(); i++){
      if(shows.getShowAt(i) instanceof Musical){
      String composer = (shows.getShowAt(i)).getComposer();
      String name = shows.getShowAt(i).getName();
      
      System.out.println(composer + " " + name);
      }
    }
                                                      
      
    
  }
  
  /**It creates a MusicalComedy object with the specified parameters, 
    * and then adds it to the given TheaterSchedule object */
  public static void addMusicalComedy(  
  TheaterSchedule <? super MusicalComedy> shows, String name, String playwrite, String composer, String theater, Date openDate){
   
    
    MusicalComedy music = new MusicalComedy(name, playwrite, composer, theater, openDate);
    
    shows.addShow(music);
    
    
    
  }
    
 
  
  
  
  
  
}//end of class