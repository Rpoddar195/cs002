/*
 * CSE017
 * Ritika Poddar
 * RIP218
 * Homework #6 Due: April 29, 2016
 * Tutor: Wellesley Arreza <wra216@lehigh.edu>
 * Program description: Sorting Tester
 */

public class SortingTester  {

  
  /** A generic implementation of insertion sort. */

  public static <E extends Comparable <E>> void insertionSort(E[] list){
    int j;
    
    for(int i =1; i<list.length; i++){
    
        E temp = list[i];
  
      for ( j = i; j >0 && list[j-1].compareTo(temp) == -1; j--) {
        list[j] = list[j-1];
      }
      list[j]=temp;
    
  } 
    
  
  
}


/**A generic implementation of merge sort. */
public static<E extends Comparable <E>> void mergeSort (E[] list){
  
    if (list.length > 1) {
      // merge sort  the first half
     
    /*  E[] firstHalf = (E[] ) new Comparable[list.length/2];
      mergeSort(firstHalf);
      
      int secondHalfLength = list.length - list.length/2;
      E[] secondHalf = (E[]) new Comparable[secondHalfLength];
       mergeSort(secondHalf);
       
        merge(firstHalf, secondHalf);
        
        */
       
    E[] firstHalf = (E[] ) new Comparable[list.length/2];
     System.arraycopy(list, 0, firstHalf, 0, list.length/2);
      
      mergeSort(firstHalf);
      
      // merge sort the second half
      int secondHalfLength = list.length - list.length/2;
      E[] secondHalf = (E[]) new Comparable[secondHalfLength];
      System.arraycopy(list, list.length/2, secondHalf, 0, secondHalfLength);
      mergeSort(secondHalf);
      
      // merge two halves
      merge(firstHalf, secondHalf);
     //System.arraycopy(temp, 0, list, 0, temp.length);
      
      
    }
  
}
  /** The recursive helper method for the merge. */ 
  private static <E extends Comparable <E>>void merge(E[] list1, E[] list2) {
    E[] temp = (E[]) new Comparable[list1.length + list2.length];
    int current1 = 0; // index in list 1
    int current2 = 0; // index in list 2
    int current3 = 0; // index in temp
    
    // as long as neither index is at the end, compare them
    // and copy the smaller value to temp
    while (current1 < list1.length && current2 < list2.length) {
      if (list1[current1].compareTo(list2[current2]) == -1){
        temp[current3++] = list1[current1++];
      }
      else{
        temp[current3++] = list2[current2++];
      }
      }
    
    // copy remaining values from list1 to temp
    while (current1 < list1.length){
      temp[current3++] = list1[current1++];
    }
    
    
    // copy remaining values from list2 to temp
    while (current2 < list2.length){
      temp[current3++] = list2[current2++];
    //return temp;
  }
  
   // return temp;
    
}
  public static<E extends Comparable <E>> void quickSort (E[] list){


   quickSort(list, 0, list.length - 1);



}
   public static <E extends Comparable<E>> void quicsSort(E[] list, int low, int high)
    {
        int lowest = low;
        int highest = high;

        if(highest>lowest)
        {
            E pivot = list[(lowest+highest)/2];
            while(lowest<highest) {
                while (lowest< highest && list[lowest].compareTo(pivot) == -1) {
                    lowest++;
                }

                while (lowest< highest && list[lowest].compareTo(pivot) == 1) {
                    highest--;
                }
                if (lowest <= highest) {
                    quickSort(list, lowest, highest);
                    highest--;
                }
            }

            if(lowest < highest)
            {
            quickSort(list,low,high);
            }
        }
    }

    public static <E extends Comparable<E>> void quickSort(E[] list, int low, int high) {
        E temp = list[low];
        list[low]=list[high];
        list[high]=temp;
    }

    
    
    /*

// A generic implementation of quick sort. 
public static<E extends Comparable <E>> void quickSort (E[] list){


   quickSort(list, 0, list.length - 1);



}
 
// The recursive helper method for quickSort(int[]) 
  public static <E extends Comparable <E>>  void quickSort(E[] list, int first, int last) {
    if (last > first) {
      int pivotIndex = partition(list, first, last);
      quickSort(list, first, pivotIndex - 1);            // sort the lower partition
      quickSort(list, pivotIndex + 1, last);             // sort the upper partition
    }
  }
   
   
  // Choose a pivot and partition the list into two sets: the elements
  //less than the pivot and the elements greater than the pivot. Place
  ///the pivot between these two sets and return its index. 
  public static<E extends Comparable <E>>  int partition(E[] list, int first, int last) {
  System.out.println("check");
    E pivot = list[first];   // we'll just select the first element, there are better ways
    int low = first + 1;
    int high = last;
    while (high > low) {
      // look for the leftmost element > pivot
      while (low <= high && pivot.compareTo(list[low]) == 1 || pivot.compareTo(list[low]) == 0){
        System.out.println("check");
        low++;
      }
      // look for the rightmost element <= pivot
        while (low <= high && pivot.compareTo(list[high]) == -1){
        System.out.println("check");
        high--;
        }
      // if we found a pair of out-of-place elements, swap them
      if (high > low) {
        E temp = list[high];
        list[high] = list[low];
        list[low] = temp;
      }
    }
    
    // find where pivot needs to be placed (move high down until it is < pivot)
    while (high > first && (list[high].compareTo(pivot) == 1 || list[high].compareTo(pivot) == 0 )){
      high--;
    }
    // swap pivot with list[high]
    if (pivot.compareTo(list[high]) == 1) {
      list[first] = list[high];
      list[high] = pivot;
      return high;
    }
    else  {                // this case is needed if the low partition is empty
      return first;
    }
  }
*/  


/** Returns an array containing the number of
random integers specified by the
parameter. */

public static Integer [] makeRandomIntList(int size){
  Integer [] tempArray = new Integer[size];
  
    for (int i =0; i <size; i++) {
      int num = (int) ((Math.random()+1)*10);
      tempArray[i] = num;
    }
    return tempArray;
  }
  
  


/** Returns an array containing the number of
random strings specified by the parameter. */

public static String [] makeRandomStringList (int size){
  
    String [] tArray = new String[size];
   
   String az = "abcdefghijklmnopqrstuvwxyz";
   //String string = "";
    for (int i =0; i <size; i ++) {
      //sizes of strings
      
      int randomNum = (int)((Math.random()*6)+3);
      
      for (int j =0; j <randomNum;  j ++) {
       
        int newRandom = (int)((Math.random()*26));
       // string += newRandom;
      tArray[j] += az.charAt(newRandom);
      }
   // tArray[i] = string;
    }
    
    return tArray;
    
  
}



 // Test the algorithm. 
  public static void main(String[] args) {
    //int[] list = {6, 2, 8, 1, 9, 15, 3, -1};
   /* String[] s = new String [3];
    s[0] = "hi";
    s[1]= "bye";
    s[2] = "f"; */
    
    //SortingTester temp = new SortingTester ();
    //go();
   int [] arraySizes = new int [4];
   arraySizes[0] = 10000;
   arraySizes[1] = 25000;
   arraySizes[2] = 50000;
   arraySizes[3] = 75000;
   
   // for list of integers
  System.out.println("Comparing sorts on lists of integers: ");
  System.out.println(" Size Insert Merge Quick ");
  
   for( int a =0; a < arraySizes.length; a++){
     int size = arraySizes[a];
     Integer [] tempArrayInt =  makeRandomIntList(size);
   
     // for int insertion sort
     
    long insertionIntSTime = System.currentTimeMillis();
    insertionSort(tempArrayInt);
    long insertionIntETime = System.currentTimeMillis();
    long insertionIntExTime = insertionIntETime -insertionIntSTime;
    //System.out.println( exTime);
    

    // for int on quick sort
    long quickIntstartTime = System.currentTimeMillis();
    quickSort(tempArrayInt);
    long quickIntendTime = System.currentTimeMillis();
    long quickIntExecTime = quickIntendTime - quickIntstartTime;
   // System.out.println( execTime);
    
   
     // for int on merge sort
    long mergeIntstartTime = System.currentTimeMillis();
    mergeSort(tempArrayInt);
    long mergeIntendTime = System.currentTimeMillis();
    long mergeIntExecTime = mergeIntendTime - mergeIntstartTime;
   
    System.out.println( size + "   " + insertionIntExTime + "   " + quickIntExecTime + "   " + mergeIntExecTime);
   }
   
   
   //for list of strings
    System.out.println("Comparing sorts on lists of integers: ");
    System.out.println(" Size Insert Merge Quick ");
    
     for( int a =0; a < arraySizes.length; a++){
    
     int sizeString = arraySizes[a];
     String [] tempArrayString = makeRandomStringList(sizeString);
  
        // for string insertion sort
    long insertionStringSTime = System.currentTimeMillis();
    insertionSort(tempArrayString);
    long insertionStringETime = System.currentTimeMillis();
    long insertionStringExT = insertionStringETime -insertionStringSTime;
   
    // for string on quick sort
    long quickStringstartTime = System.currentTimeMillis();
    quickSort(tempArrayString);
    long quickStringendTime = System.currentTimeMillis();
    long quickStringexecTime = quickStringendTime -quickStringstartTime;
    
       
    // for string on merge sort
    long mergeStringstartTime = System.currentTimeMillis();
    mergeSort(tempArrayString);
    long mergeStringendTime = System.currentTimeMillis();
    long mergeStringexecTime = mergeStringendTime -mergeStringstartTime;
    
    System.out.println( sizeString + "   " + insertionStringExT + "   " + quickStringexecTime + "   " + mergeStringexecTime);
  
  // make random string a int list
    // 4 string 4 int
    
  
  } 
  


  }




}// end of class
