/*
CSE 17
Ritika Poddar
RIP 218
Program #1 DEADLINE: February 20, 2016
Program Description: Search Engine Document */

import java.util.*;

class Document{
  Term [] terms;
  double magnitude = 0;
  
  public static void main (String args[]){
   /* main method: takes user input, creates Term object by calling the methods, prints the object,
    * and the prints the magnitude*/
 
     //User Input
     Scanner userInput = new Scanner (System.in);          
     System.out.println("Please enter a line of text");
     String firstInput = userInput.nextLine();
  
     /*  turn to lowercase and turn into array based on where the spaces in string are*/
    String [] unsortedArray = stringConstructor(firstInput);
     
     /*sort the string input alphabetically */
    String [] sortedArray = sort(unsortedArray);
  
     
     /*Create the Term object */
   //   indexContent (sortedArray);
      
     /* print out the object and the magnitude */ 
   
  }
  
  public static String []stringConstructor(String firstInput){
 String lowercase = firstInput.toLowerCase(); // convert text to lower case
 String [] splitUp = lowercase.split(" "); // split to separate words  
//index ?
 return splitUp;
}
  
 private static  String [] sort( String [] unsortedArray){
 /*Selection sort to create an alphabetically sorted array */
   
   for (int i = 0; i< unsortedArray.length -1; i++){
    String currentMin = unsortedArray[i];
    int currentMinIndex = i;
     
    for (int j =i +1; j < unsortedArray.length; j++){
      if(currentMin.compareTo (unsortedArray[j]) > 0){
        currentMin = unsortedArray[j];
        currentMinIndex = j;
      }
      
    }
    
    if (currentMinIndex != i) {
      unsortedArray[currentMinIndex] = unsortedArray[i];
      unsortedArray[i] = currentMin;
    }
  }
  
 return unsortedArray;
 
  
}
 
  private static int countDistinctStrings( String [] sortedArray){

    int counter = 0;
    
  for(int i =0; i< sortedArray.length-1; i++){
      if(sortedArray[i].compareTo(sortedArray[i+1]) == 0){
        counter ++;
        
      }
  

}
   return counter;

  }
  
  private void indexContent ( String [] sortedArray){
    /*this method counts the frequency of each word and creates a terms array made of Terms elements 
     * based on how many distinct words there are */
  //first determine how big terms object will be: 
    int  distinctStrings = countDistinctStrings(sortedArray);
   
   //Create terms array of proper size
      Term [] terms = new Term [distinctStrings];
    
   // put only the unique strings in terms
      int counter = 0;
      int sum =0;
      for(int i =1; i< sortedArray.length-1; i++){
      if(sortedArray[i].compareTo(sortedArray[i+1]) == 0){
        counter ++;
        sortedArray [i] = null;
        
        
        for (int j = i; j< sortedArray.length - 1; j++){
          sortedArray[j] = sortedArray[i +1];
       
      }
      
      }
      terms[i].setFreq(counter);
      sum += Math.pow(counter, 2);
      
      counter = 0;
      }
      
      for (int x = 0; x< sortedArray.length -1; x++){
        terms[x]= sortedArray[x];
    
      }
       
      
      double themagnitude = Math.sqrt(sum);
     System.out.print(themagnitude);
       
     }

  
     
  
public Term[] printTermFreqs(){
  for (int x = 0; x< terms.length-1; x++){
       System.out.println (terms[x] + "   "  );
     }
}

}
      

 class Term{
  String word;
  int freq; // represents the number of times the word appears in the  document
  
  
    public Term(String aWord, int theFreq){
    word = aWord;
    
    freq = theFreq;
    
    }
    
    public String getWord (){
      return word;
    }
    
    public int getFreq(){
      return freq;
    }
    
    public void setFreq (int n){
      freq = n;
    }
    
}
  
