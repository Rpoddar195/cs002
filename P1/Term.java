/*
CSE 17
Ritika Poddar
RIP 218
Program #1 DEADLINE: February 20, 2016
Program Description: Search Engine Document */

public class Term{
  String word;
  int freq; // represents the number of times the word appears in the  document
  
  
    public Term(String aWord, int theFreq){
    word = aWord;
    
    freq = theFreq;
    
    }
    
    public String getWord (){
      return word;
    }
    
    public int getFreq(){
      return freq;
    }
    
    public void setFreq (int n){
      freq = n;
    }
    
}