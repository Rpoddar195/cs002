/* Ritika Poddar
 * CSE 017
 * Due 3/10/16
 * */
import java.util.Scanner;
import java.io.File;
import java.util.Arrays;
import java.util.*;


public class AnimalHospital{
  private Pet[] pets;
  
  public AnimalHospital(){
  
  }
  /**Reads pet descriptions from the file specified by
the parameter and populates pets*/
 public void readData(File filename) throws Exception{
  
   Scanner code = new Scanner (filename);
   int numbOfPets = Integer.parseInt( code.next()); 
   Pet[] pets  = new Pet[numbOfPets];
   int counter = 0;
   
   while(code.hasNext()){
        if((code.next()).equals("CAT")){       
          String petname = code.next();       
          String ownername = code.next();       
          String petsex = code.next();       
          int petage = code.nextInt();       
          double petweight = code.nextDouble();            
          boolean spayed = code.nextBoolean();
         // boolean declawed = code.nextBoolean();
          pets[counter] = new Cat(petname, ownername, petsex, petage, petweight, spayed);                          
         
          counter++;
        
        } 
        if((code.next()).equals("BIRD")){       
          String petname = code.next();       
          String ownername = code.next();       
          String petsex = code.next();       
          int petage = code.nextInt();       
          double petweight = code.nextDouble();       
          boolean deC = code.nextBoolean(); 
          
          pets[counter] = new Bird(petname, ownername, petsex, petage, petweight, deC);       
              
        counter++;
        }       
         if((code.next()).equals("DOG")){          
           String petname = code.next();       
               String ownername = code.next();       
               String petsex = code.next();       
               int petage = code.nextInt();       
               double petweight = code.nextDouble();       
               boolean deC = code.nextBoolean();      
                pets[counter] = new Dog(petname, ownername, petsex, petage, petweight, deC);       
              
        counter++;  
         } 
        
 }// end of while loop
 code.close();   
 
 printPetList(pets); 
 printPetInfoByName(pets, "Spot");
 Pet p = new Pet("Spot", "Dick", "M", 6, 63.4);
 System.out.print("Spot's index: ");
  System.out.println( getPetIndex( pets, p));  
 
 printPetInfoByOwner(pets, "Dick");  
  
 
 }// end of method
  
/* public Pet[] getpets(){
   return pets;
 }*/
  
  /**Print out the contents of pets using the toString method
of each element */
  public void printPetList(Pet [] pets){
    for(int i =0; i< pets.length-1; i++){
      System.out.println(pets[i].toString());
       }
    
                               
  }
  
  /**Print out all pets with the specified name, using their
toString methods. */
  public void printPetInfoByName(Pet[] pets, String name){
   System.out.println("Pets named " + name + " : ");
   
    for(int i = 0; i< pets.length-1; i++){
      if((pets[i].getName()).equals(name)){
        System.out.println(pets[i].toString());
    } 
  } 
  
  }
  /**Print out all pets with the specified owner, using their
toString methods.*/
  public void printPetInfoByOwner( Pet[] pets, String name){
  System.out.println("Pets owned by " + name + " : ");
   
    for(int i = 0; i< pets.length-1; i++){
      if((pets[i].getOwner()).equals(name)){
        System.out.println(pets[i].toString());
    }
  } 
  }
   /**Return the index of the first pet p in pets or -1 if not
found. Two pets are considered the same if they
have the same name and same owner. */
  public int getPetIndex( Pet[] pets, Pet p){
  int petindex =0;
  for(int j =0; j< pets.length-1; j++){
    if((pets[j].getName()).equals(p.getName()) && (pets[j].getOwner()).equals(p.getOwner())){
     petindex = j;
      return petindex;
    }
  }
  return -1;
  }
  
  public static void main (String[] args) throws Exception{
  
    if (args.length != 1 ) {
      System.out.println("Error");
    System.exit(0);
    }
    
    else{ 
    File myFile = new File(args[0]);
  
    AnimalHospital read = new AnimalHospital();
    //String filename = read.toString(myFile);
    
    read.readData(myFile);
   
 //   Pet[] array = read.getpets();
    
    
  
   
  
  }
  
   
    
  
    
  
}
  

  
}// end of class
