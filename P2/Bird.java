/* Ritika Poddar
 * CSE 017
 * Due 3/10/16
 * */


public class Bird extends Pet{
  private boolean clipped;
 
  /**initialize*/
  public Bird(String name, String owner, String sex, int age, double weight, boolean clipped){
     super ( name, owner, sex, age, weight);

     this.clipped = clipped;

 
 }
  
    /**initialize*/
 public Bird(String name, String owner, String sex, int age, double weight){
 
 super(name,  owner,  sex,  age,  weight);
 this.clipped = false;

 
 }
  
 /**getter*/
  public boolean isClipped(){
    return false;
  }
  
  /** setters*/
  public void setClipped(boolean clipped){
    this.clipped = clipped;
  }
  
  /**getter*/
  public String getSize( ){
    if(weight > 2){
      return "Large";
    }
    else if (weight < 0.1){
      return "Small";
    }
    else{
      return "Medium";
    }
 }
  
  /** print out constructor */
  public String toString(){
    String clip = " ";
    if(isClipped() == false){
      clip = "(not clipped)";
    }
    else if(isClipped() == true){ 
     clip = " ";
    }
 return name + " owned by  " + owner + ": " + getSize() + " " + "Bird" + ", " + sex + ", Age " + age + " " + weight + " lbs " + clip ;
   
 }
  
  
  
  
  
}