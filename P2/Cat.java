/* Ritika Poddar
 * CSE 017
 * Due 3/10/16
 * */

public class Cat extends Pet{
  
private boolean declawed;
/** initialize*/
public Cat(String name, String owner, String sex, int age, double weight, boolean spayedNeutered){
 super( name,  owner,  sex,  age,  weight,  spayedNeutered);
 this.declawed = false;

 
 }
 /** initialize*/
 public Cat(String name, String owner, String sex, int age, double weight){
 super(name,  owner,  sex,  age,  weight);
 this.declawed = false;

 
 }
  /** getters*/
 public boolean isDeclawed(){
   return false;
 }
 /** setter*/
 public void setDeclawed( boolean declawed) {
   
 }
   /** getters*/
 public String getSize( ){
    if(weight > 11){
      return "Large";
    }
    else if (weight < 9){
      return "Small";
    }
    else{
      return "Medium";
    }
 }
 /** print out constructor */
 public String toString(){
   String claws = " "; 
   if(isDeclawed() == false){
      claws = "(not declawed)";
    } 
    else if(isDeclawed() == true){ 
     claws = " ";
    } 
 return name + " owned by  " + owner + ": " + getSize() + " " + "Cat" + ",  " + sex + ", Age " + age + ",  " + weight + " lbs " + spayedNeutered + " " + claws;
   
 }
  
  
  
}
