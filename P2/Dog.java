/* Ritika Poddar
 * CSE 017
 * Due 3/10/16
 * */

public class Dog extends Pet{
  /**initialize*/
  public Dog(String name, String owner, String sex, int age, double weight, boolean spayedNeutered){
 super( name,  owner,  sex,  age,  weight,  spayedNeutered);
 
 }
 
  /** initialize*/
 public Dog(String name, String owner, String sex, int age, double weight){
 super( name,  owner,  sex,  age,  weight);
 
 }
 /** print out constructor */
  public String getSize( ){
    if(weight > 50){
      return "Large";
    }
    else if (weight < 30){
      return "Small";
    }
    else{
      return "Medium";
    }
 }
 
 public String toString(){
 return name + " owned by  " + owner + ": " + getSize() + " " + "Dog" + ",  " + sex + ", Age " + age + ",  " + weight + " lbs " + spayedNeutered ;
   
 }
 
  
  
  
}