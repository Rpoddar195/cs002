/* Ritika Poddar
 * CSE 017
 * Due 3/10/16
 * 
 * */
 
 
 public class Pet{
 
 String name;
  String owner;
 String sex;
  int age;
 double weight;
 boolean spayedNeutered;
 
 /**Initialize fields */
 public Pet(String name, String owner, String sex, int age, double weight, boolean spayedNeutered){
 this.name = name;
 this.owner = owner;
 this.sex = sex;
 this.age = age;
 this.weight = weight;
 this.spayedNeutered = spayedNeutered;

 
 }
 /** initiaze fields but with spayed neutered set as false*/
 public Pet(String name, String owner, String sex, int age, double weight){
 this.name = name;
 this.owner = owner;
 this.sex = sex;
 this.age = age;
 this.weight = weight;
 this.spayedNeutered = false;

 
 }
 
 /** getter*/
 public String getName(){
   return name;
 }
 
 /** getter*/
 public String getOwner(){
   return owner;
 }
  /** getter*/
 public String getSex(){
   return sex;
 }
  /** getter*/
 public int getAge(){
   return age;
 }
  /** getter*/
 public double getWeight(){
   return weight;
 }
  /** getter*/
 public boolean isSpayedNeutered(){
   return spayedNeutered;
 }
  /** setter*/
 public void setAge(int age){
   this.age = age;
 }
   /** setter*/
 public void setWeight(double weight){
   this.weight = weight;
 }
   /** setter*/
 public void setSpayedneutered(boolean spayedNeutered){
   this.spayedNeutered = spayedNeutered ;
 }
   /** getter*/
 public String getSize( ){
   return "Medium";
 }
  /** print out constructor */
 public String toString(){
 return name + " " + owner + " " + getSize() + " " + "Pet" + ", " + sex + ",Age " + age + ",  " + weight + " lbs ";
   
 }
 
 
 
 
 
 
 
 
 
 
 }