/*
 * CSE017
 * Ritika Poddar
 * RIP218
 * Program #3 Due: April 5, 2016
 * Tutor: Wellesley Arreza <wra216@lehigh.edu>
 * Program description: AsciiDisplay
 */
import java.util.ArrayList;
import java.io.File;
import java.util.Scanner;
import java.io.*;
import java.util.*;
/**A window that characters can be �drawn� onto*/

public class AsciiDisplay{
  
  private char [] [] grid;
  private ArrayList<Shape> shapes;
  
  /** constructor*/
   public AsciiDisplay(){
    shapes = new ArrayList<Shape>();
    grid = new char [30] [15];
  }
  
  /**Add a Shape object to shapes*/
  public void addShape(Shape s){
    shapes.add(s);
  }
  
  /**Returns the first Shape in shapes with the given id, or null*/
  public Shape findShape(String id){
   for(int i = 0; i< shapes.size(); i++){
    
      if((shapes.get(i)).equals(id)){
         return shapes.get(i);
      }
      
    }
     return null;
        
    
  }
  
  /**Moves the shape with the given id to newLoc and returns 0
    * If the shape is not in shapes then returns -1. */
  public int moveShape(String id, Coordinate newLoc){

    for(int i = 0; i< shapes.size(); i++){
    
      if(((shapes.get(i)).getId()).equals(id)){
         (shapes.get(i)).move(newLoc);
         return 0;
      }
      
    }
 
        return -1; 
   
   
   
  }
  
  /**Change the drawCharacter for the shape with given id to char c and returns 0. 
    * If the shape is not in shapes then returns -1. */
  public int changeChar( String id, char c){
  
    Shape x = findShape(id);
    if (x == null) {
      return -1;
    }
    if (x instanceof Rectangle) {
      Rectangle a = (Rectangle)x;
      a.setDrawCharacter(c);
      return 0;
    }
    else if (x instanceof Point) {
      Point b = (Point)x;
      b.setDrawCharacter(c);
      return 0;
    }
    else {
      return -1;
    }
  

  }
  
  /**Deletes the objects with given id from shapes and returns 0. 
    * If the shape is not in shapes then returns -1. */
  public int deleteShape(String id){
     for(int i = 0; i< shapes.size(); i++){
    //((shapes.get(i)).getId()).equals(id)
      if(id.equals(shapes.get(i).getId())){
        shapes.remove(shapes.get(i));
         return 0;
      }
     
    }
     
     return -1;
  }
  
  /**Calls updateGrid() and then prints the contents of grid*/
  public void printGrid(){
    updateGrid();
   
    System.out.println("+-----------------------------+");
    for(int i =0; i < 15 ; i++){
      System.out.println("|");
      for(int j =0; j< 30; j++){
        System.out.print(grid[j][i]);
   
      }
       System.out.println("|");
      } 
    System.out.println("+-----------------------------+");
  }
 
  /**Puts a character c at a specific (x,y) location on the grid. */
  void putCharAt(int x, int y, char c){
    grid[x][y] = c;
  }
  
  /**Clears grid and �draws� each element of shapes in it.*/
  private void updateGrid(){
   char fill = ' ';
    for(int i = 0; i < 30; i++){
        for(int j = 0; j < 15; j++){
        grid[i][j] = fill;
        }
    }
        
    for(int i =0; i< shapes.size(); i++){
   // AsciiDisplay dis = read.(shapes.get(i));
     shapes.get(i).draw(this );
    }
  }
  
  /**main method*/
  public static void main (String[] args){
     
    
    if (args.length != 1 ) {
      System.out.println("Error");
    System.exit(1);
   }
    else{ 
    File inFile = new File(args[0]);
    AsciiDisplay read = new AsciiDisplay();
    
   
    try {
    Scanner code = new Scanner(inFile);
    
   
    while(code.hasNext()){
     
      try{
        String command = code.next();
        
        if(command.equals("P")){
       
         String id = code.next();       
         int x = code.nextInt();    
         int y = code.nextInt();
         
         Coordinate newCoord = new Coordinate(x,y);
         Point newPoint = new Point(id, newCoord);
         //System.out.println(newPoint.id + " " + newPoint.location);
        // System.out.println(id +" " + x + " " + y);
         read.addShape(newPoint);
       
        }
        
       else if( command.equals("R")){
       
        String id = code.next();       
         int x = code.nextInt();    
         int y = code.nextInt();
         int length = code.nextInt();
         int height = code.nextInt();
         
         Coordinate newCoord = new Coordinate(x,y);
         Rectangle newRec = new Rectangle(id, newCoord, length, height);
        // System.out.println(id +" " + x + " " + y + " " + length + " " + height);
         read.addShape(newRec);
        }
       
     else if( command.equals("T")){
        String id = code.next();       
         int x = code.nextInt();    
         int y = code.nextInt();
         String content = code.nextLine(); 
         
         Coordinate newCoord = new Coordinate(x,y);
         TextBox newText = new TextBox(id, newCoord, content);
        //  System.out.println(id +" " + x + " " + y + " "+ content);
         read.addShape(newText);
         
        }
        
     else if( command.equals("M")){
        
         String id = code.next();       
         int x = code.nextInt();    
         int y = code.nextInt();
         
         Coordinate newCoord = new Coordinate(x,y);
        // System.out.println(id +" " + x + " " + y);
         read.moveShape(id, newCoord);
          
       }
      else if( command.equals("C")){

           String id = code.next();       
           char c = code.next().charAt(0);    
           read.changeChar(id, c);
        
         }
        
      else if( command.equals("E")){
   
           String id = code.next();       
          // System.out.println(id );
           read.deleteShape(id);
         }
        
     else  if( command.equals("D")){
  
          read.printGrid();
       }
       
    
     else  {
       String invalid = code.nextLine();  
       System.out.println( "ERROR: Invalid command " + command);
     
       }
      
    }
     catch(InputMismatchException ex){
          System.out.println("ERROR invalid argument. Skipping line.");
          code.nextLine();
        } 

    }
    
    
    }
    
    catch (java.io.FileNotFoundException ex){
      System.out.println("ERROR: File not found");
    } 

    
    }
  
  
  
  }// end of main method
  
}// end of class
  
  
  
  
    
