/*
 * CSE017
 * Ritika Poddar
 * RIP218
 * Program #3 Due: April 5, 2016
 * Tutor: Wellesley Arreza <wra216@lehigh.edu>
 * Program description: AsciiDisplay
 */
import java.util.ArrayList;
import java.io.File;
import java.util.Scanner;
import java.io.*;
import java.util.*;
/**A simple class representing an (x,y) coordinate on the display*/
public class Coordinate{
  
  private int x; //x position, distance from left
  private int y;// y position, distance from top
  
  /**Constructor*/
  public Coordinate(int x, int y ){
   this.x = x;
   this.y = y;
    
  }
  
  /**getter*/
  public int getX(){
    
    return x;
  }
  
    /**getter*/
  public int getY(){
    
    return y;
  }
  
  
  
  
  
  
  
  
  
  
}// end of class
