/*
 * CSE017
 * Ritika Poddar
 * RIP218
 * Program #3 Due: April 5, 2016
 * Tutor: Wellesley Arreza <wra216@lehigh.edu>
 * Program description: AsciiDisplay
 */
import java.util.ArrayList;
import java.io.File;
import java.util.Scanner;
import java.io.*;
import java.util.*;
/**A single point.*/
public class Point extends Shape implements CharacterGraphic{
  
  private char drawCharacter = '*'; //character used to draw the point. Defaults to �*�.
  
  
  /** constructor*/
  public Point(String id, Coordinate location){
  super(id, location);
  this.id = id;
  this.location = location;
     
  }
  
  
  
  /**Draws drawCharacter at location on the grid of dis.*/
  public void draw(AsciiDisplay dis){
    // need to first get x, y , c from the object
    
     dis.putCharAt(location.getX(), location.getY(), drawCharacter);
        
      
  }
  
  /** setter*/
  public void setDrawCharacter( char c){
    this.drawCharacter = c;
    
  }
  
  
  
  
  
}