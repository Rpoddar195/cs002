/*
 * CSE017
 * Ritika Poddar
 * RIP218
 * Program #3 Due: April 5, 2016
 * Tutor: Wellesley Arreza <wra216@lehigh.edu>
 * Program description: AsciiDisplay
 */
import java.util.ArrayList;
import java.io.File;
import java.util.Scanner;
import java.io.*;
import java.util.*;
/** a rectangle*/
public class Rectangle extends Shape 
  implements CharacterGraphic{
  private int length; //horizontal dimension of the rectangle
  private int height;  //vertical dimension of the rectangle.
  private char drawCharacter = '#'; //character used to fill the rectangle. Defaults to �#�.
  
  /** constructor*/
  public Rectangle(String id, Coordinate location, int length, int height){
    super(id, location);
  
  this.length = length;
  this.height = height;
  }
  
 /**Updates the grid of dis by drawing a filled-in rectangle*/
  public void draw(AsciiDisplay dis){
  
  // need to first get x, y , c from the object
    // call putCharAt(x,y,c); to put the points into grid.
    //do it as a loop to get it in as h and l
    for(int q=0; q<length; q++){
      for(int p=0; p<height; p++){
       
        dis.putCharAt(location.getX()+q, location.getY()+p, drawCharacter);
        
      }
  }
  }
  
  /** setter*/
  public void setDrawCharacter( char c){
    this.drawCharacter= c ;
    
  }
  
  
  
  
  

  
  
  
  
  
  
  
  
  
  
}