/*
 * CSE017
 * Ritika Poddar
 * RIP218
 * Program #3 Due: April 5, 2016
 * Tutor: Wellesley Arreza <wra216@lehigh.edu>
 * Program description: AsciiDisplay
 */

import java.util.ArrayList;
import java.io.File;
import java.util.Scanner;
import java.io.*;
import java.util.*;
/**This is the class of all of the objects that can be drawn to the display*/
public abstract class Shape{
  protected String id;
  protected Coordinate location;
  
  /** constructor*/
  public Shape(String id, Coordinate location){
  
  this.id = id;
  this.location = location;
  }
  
  
  
  /** getter*/
  public String getId(){
    
    return id;
  }
  
  /**Draws the shape on the grid of dis*/
  abstract void draw (AsciiDisplay dis);
  
  /**Change�s the object�s location to newLoc.*/
  public void move(Coordinate newLoc){
    this.location = newLoc;
  }
  
  /**Returns true if the actual parameter is a Shape with the same id.*/
  public boolean equals(Object obj){
    
    return true;
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
}// end of class