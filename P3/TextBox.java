/*
 * CSE017
 * Ritika Poddar
 * RIP218
 * Program #3 Due: April 5, 2016
 * Tutor: Wellesley Arreza <wra216@lehigh.edu>
 * Program description: AsciiDisplay
 */
import java.util.ArrayList;
import java.io.File;
import java.util.Scanner;
import java.io.*;
import java.util.*;


public class TextBox extends Shape{
  
  private String content; // The characters to show in the text box.
  
  /** Constructor. */
  public TextBox(String id, Coordinate location, String content){
     super(id, location);
     this.content = content;
  }
  
  /**Updates the grid of dis by writing the string horizontally starting from the specified location.*/
  public void draw(AsciiDisplay dis){
    for(int i = 0; i< content.length(); i++){
    dis.putCharAt(location.getX() +i, location.getY(), content.charAt(i));
    }
  }
  
  
  
}