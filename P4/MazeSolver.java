/*
 * CSE017
 * Ritika Poddar
 * RIP218
 * P#4 Due: April 17, 2016
 * Tutor: Wellesley Arreza <wra216@lehigh.edu>
 * Program description: Maze Solver
 */


import java.util.ArrayList;
import java.io.File;
import java.util.Scanner;
import java.io.*;
import java.util.*;

public class MazeSolver{
  
  /**This method solves the maze passed in as a parameter. */
 
  public static ArrayList<Cell> findPath(Maze currentMaze){
    
    
    Cell current = currentMaze.getStartCell(); 
    ArrayList <Cell> path = new ArrayList <Cell> ();
    
    path.add(currentMaze.getStartCell());
    
    ArrayList <Cell> x = findPath(currentMaze, current, path);
    if( x == null){
    
      return null;
    } 
    
    return path;
    
   
  }
    
  
  /** This method recursively finds a path from the start of the currentMaze to its end that goes through the
current Cell. */
  private static ArrayList<Cell> findPath(Maze currentMaze, Cell current, ArrayList<Cell> path ){
   ArrayList <Cell> nextCell = currentMaze.getNeighbors(current);
  
   
   if(nextCell.size() == 0){
    
      return null;
    }   
      
     else if(current == currentMaze.getEndCell()){
      path.add(current); 
      return path;
       }
      
     
   
    //remove cells aready checked
    for(int i = 0;i<path.size();i++){
      
        if(nextCell.contains(path.get(i))){       
          nextCell.remove(path.get(i));
        }
    }
    
   /** 
    //Find rest of the path
    for(int i =0; i<nextCell.size(); i++){
      if(nextCell.size() == 1){
        current = nextCell.get(i);
        path.add(current);
      
      if((findPath(currentMaze, current, path)) != null){
      
        return path;
      }
      }
      
      else if(nextCell.size() >1){
          if((findPath(currentMaze, current, path)) == null){
      
        path.remove(current);
        continue;
      }
        
      
      }
    }
    return null;
    
    */
    
    
     //Find rest of path
      for(int i =0; i< nextCell.size(); i++){
      
      current = nextCell.get(i);
      path.add(current);
      
      
     ArrayList <Cell> a = findPath(currentMaze, current, path);
      if( a != null){
      
        return path;
      }
     
      else if(a  == null){
      
        path.remove(current);
        continue;
      }
    }
      return null;  
    
   
  }
  
  
  /** This method prints out the solution to the maze */
  public static void printSolvedMaze( Maze m, ArrayList<Cell> solution){
    char[][] myDisplay = m.getMazeDisplay();
    int column = 0;
    int row = 0;
    
    
    //replace empty space with "."
     for(int i = 0;i < solution.size();i++){
    
      row = (solution.get(i)).getDisplayRow();
      column = (solution.get(i)).getDisplayCol();
      myDisplay[column][row] = '.';
    }
     
     for(int i = 0;i<solution.size()-1;i++){
        row = (solution.get(i)).getDisplayRow();
        column = (solution.get(i)).getDisplayCol();
       
        int nextrow = (solution.get(i+1)).getDisplayRow();
        int nextcolumn = (solution.get(i+1)).getDisplayCol();
       
      if(row == nextrow){
       int col = (column + nextcolumn) / 2;
        myDisplay[col][row] = '.';
      }
      else if(column == nextcolumn){
        row = (row + nextrow) / 2;
        myDisplay[column][row] = '.';
      }
    }
    
    
    m.printMaze(myDisplay);
    
    
  }
  
  public static void main (String [] args){
    int mazeId = 0;
    if(args.length == 0){
    
      mazeId = 1;
    }
    else if(args.length >= 2){
    
      System.out.println("ERROR");
      System.exit(1);
    }
   
    else{
     // File inFile = new File(args[0]);
      try{
      
        mazeId = Integer.parseInt(args[0]);
      }   
      
      
      catch(InputMismatchException ex){
      
        System.out.println("ERROR: Invalid input. Please enter integer.");
        System.exit(0);
      }
    }
    
    Maze maze = new Maze(20 ,6 ,mazeId);
    
    System.out.println("Maze: ");
    maze.printMaze();
    System.out.println(" ");
    System.out.println("Solution: ");
    
    ArrayList <Cell> solution = findPath(maze);
    printSolvedMaze(maze, solution);
  }
    
    
    
   
  
  
  
  
}// end of class