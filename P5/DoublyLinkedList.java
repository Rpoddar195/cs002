/*
 * CSE017
 * Ritika Poddar
 * RIP218
 * Program #5 Due: May 8, 2016
 * Tutor: Wellesley Arreza <wra216@lehigh.edu>
 * Program description: Doubly Linked
 */
import java.util.ArrayList;
import java.io.File;
import java.util.Scanner;
import java.io.*;
import java.util.*;

public class DoublyLinkedList <E> {
  
  
  private DoublyLinkedNode <E> head; //first node in list
  private DoublyLinkedNode <E> tail; //last node in list
  private int size; //number of elements in list
  
 //DoublyLinkedList <E> doubleList;
 
  /** creates empty DoublyLinkedList*/
  public DoublyLinkedList(){
     //doubleList = new DoublyLinkedList <E> ();
    this.head = null;
    this.tail = null;
    this.size = 0;
    
  }
  
  /**Returns the node with the given index*/
  private DoublyLinkedNode <E> getNode(int index){
    
    int middle = size/2;
   
    if (index<= middle) {
           
      DoublyLinkedNode<E> x = head;
      
      for (int i = 0; i < size; i++) {
                if(i != index) {
                     x = x.getNext();
                }
                
                else {
                    return x;
                }
            }
        } 
    
    else {
            DoublyLinkedNode<E> y = tail;
            
            for (int i = size-1; i > 0; i--) {
               
              if(i!=index){
                    y = y.getPrevious();
                } 
              
              else {
                    return y;
                }
            }
        }
        return null;
    
  }
  
  /**Adds a DoublyLinkedNode containing
element to the end of the list. */
 public void add(E element){
   
   DoublyLinkedNode <E> newList = new DoublyLinkedNode <E> (element); 
        
    if(size == 0){  //no elements in list yet
     head = newList;
     tail = newList;
      
    }
    
    else{
     tail.setNext(newList);
     newList.setPrevious(tail);
     newList.setNext(null);
     
     tail= newList;
    }
    
    size++;
      
    
  } 
  
  /** Inserts a DoublyLinkedNode at index
containing element.  */
  public void add(int index, E element){
   
    DoublyLinkedNode <E> newList = new DoublyLinkedNode <E> (element);
    try{
      if( index >= 0 && index < size - 1){
     
     boolean x = false;   
     if (index == 0) {
      
       // if only element in list
       if (newList.getNext() == null) {
          add(element);
          size++;
          x = true;
        }
   
       // to put it at the beginning of the list
       else{
          head.setPrevious(newList);
          newList.setNext(head);
          head = newList;
         size++; 
         
       }
     }
     
     //if index is at the end of the list
     else if (index > 0  && newList.getNext() == null){
       
          add(element);
          size++;
       }
    
     // to put it in the middle of the list
    else {
      
       DoublyLinkedNode<E> current = getNode(index);
       newList.setPrevious(current.getPrevious());
       current.getPrevious().setNext( newList);
      current.setPrevious(newList);
        newList.setNext(current);
       size++;
  
       
    }
 
     if(x = false) {
            size++;
        }
  }
      
    }
    
    catch (IndexOutOfBoundsException ex) {
      
      System.out.println("Invaid index" + index + " for element " + element);
    }
    
  }
  
  /** Returns the value of the node at index in the
list. */
  public E get(int index){
     if(index<0 || index > size-1)
            throw new IndexOutOfBoundsException("Invalid index:" + index);
        return getNode(index).getElement();
  }
  
  
  /**Returns the index of the node containing
�element�, or -1 if the element is not in the list.*/
  public int indexOf(E element){
     
    for (int i = 0; i < size; i++) {
            
      if(getNode(i).getElement().equals(element)) {
                return i;
            }
        }
        return -1;
    
  }
  
  
  /**Removes the Node at the given index and
return its element*/
  public E remove(int index){    
    if (index > size-1 || index < 0) 
     throw new IndexOutOfBoundsException(" Invalid index: " + index);
    {
     
  DoublyLinkedNode<E> temp = getNode(index);
    if (index == 0) {
          temp = head;
          head = head.getNext();
          size--;
        
          if(size == 0){
           head = null;
           tail = null;
         }
         
         return temp.getElement();
    }
    
   else if (index == size - 1) {
    temp = tail;
     temp.getPrevious().setNext(null);
     temp = null;
      size--;
     return temp.getElement();
    }
  
   else {
      temp = head;
     
      for( int i =0; i < index; i ++){
        temp = temp.getNext();
        
      }
       
       DoublyLinkedNode<E> y = temp.getNext();// the node after index
       DoublyLinkedNode<E> x = temp.getPrevious(); // the node before index
       // connect the previous node and the next node together
       x.setNext(y) ; 
       y.setPrevious(x);
      
       
       size--;
       return temp.getElement();
      
    }
    
    }
    
  }  
    
  
  
  
  /**Removes the first Node that contains element.
Returns true if the element was successfully
removed, or false if no such element is in the
list. */
   public boolean remove(E element){

   
     int index = indexOf(element);

        if(index >=0) {
          
          remove(index);
            return true;
        } 
        
        else {
            return false;
        }
    
    
  }
  
   /**Returns the number of Nodes in the list*/
   public int size(){
     return size;
     
   }
  
  
  
  
  
}// end of class