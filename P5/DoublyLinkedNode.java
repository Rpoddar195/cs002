/*
 * CSE017
 * Ritika Poddar
 * RIP218
 * Program #5 Due: May 8, 2016
 * Tutor: Wellesley Arreza <wra216@lehigh.edu>
 * Program description: Doubly Linked
 */
import java.util.ArrayList;
import java.io.File;
import java.util.Scanner;
import java.io.*;
import java.util.*;


public class DoublyLinkedNode <E>{
  
  private E element; // the content of the node
  
  private DoublyLinkedNode <E> previous; //A reference to the previous node in the list,
                                         //null if it is the first Node
  
  private DoublyLinkedNode <E> next; //next node in the list, null is last Node
  
  
  /**Sets the element field to the parameter and
initializes the previous and next fields to null. */
  public DoublyLinkedNode( E element){
    this.element = element;
    this.next = null;
    this.previous = null;
    
  }
  
  /**Returns the previous node that this node is
connected to*/
  public DoublyLinkedNode<E> getPrevious(){
    
    return previous;
  }
  
  /**Returns the next node that this node is
connected to. */
  public DoublyLinkedNode<E> getNext(){
    
    return next;
  }
  
 /**Sets the previous node for this node. */
  public void setPrevious(DoublyLinkedNode <E> node){
    this.previous = node;
    
  }
  
  /**Sets the next node for this node. */
  public void setNext (DoublyLinkedNode <E> node){
    
    this.next = node;
  }
  
  /** returns content on node*/
  public E getElement(){
    return element;
    
  }
  
  /** sets the node's content*/
  public void setElement(E value){
       this.element = value;
  }
  
  
  
  
  
}// end of class