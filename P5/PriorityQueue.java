/*
 * CSE017
 * Ritika Poddar
 * RIP218
 * Program #5 Due: May 8, 2016
 * Tutor: Wellesley Arreza <wra216@lehigh.edu>
 * Program description: Doubly Linked
 */

import java.util.ArrayList;
import java.io.File;
import java.util.Scanner;
import java.io.*;
import java.util.*;

public class PriorityQueue <E extends Comparable<E>> {
    private DoublyLinkedList<E> elements;
  
    /** constructor */
    public PriorityQueue() {
        elements = new DoublyLinkedList<E>();
    }

    /**Adds an elements to the priority queue */
    public void enqueue(E element){
        boolean x = true;
      
        if(size() == 0) {
          elements.add(element);
          x = false;
        }
        
        for (int a = 0; a < elements.size(); a++) {
            if (element.compareTo(elements.get(a)) < 0) {
             // int index = elements.indexOf(element);  
              elements.add(a, element);
              
                x = false;
                break;
            }
        }
        if (x == true){
          
          elements.add(element);
        }
    }

    /**Returns the element at the front of the priority queue without removing it
     * If the queue is empty, it returns null value.*/
    public E peek() {
      if (size() > 0) {
        return elements.get(0);
      }
      
     return null;
    }

    /**Returns and removes the element at the front of the priority queue 
     * return null if empty */
    public E dequeue()
    {
        return elements.remove(0);
    }

    /**Removes all elements from priority queue  */
    public void clear(){
        for (int i = 0; i < size(); i++) {
            elements.remove(i);
        }
    }

    /**Returns the number of elements in the priority queue (size of queue) */
    public int size() {
        return elements.size();
    }

    /** Main method. Adds elementsand then prints them out from the priority queue, one per line.*/
    public static void main(String[] args) 
    {
      try{
        if(args.length !=1) {
          //
          System.out.print("ERROR: no file found");
            System.exit(1);
        }


        String myinput = args[0];
        File myfile = new File(myinput);
        Scanner scanner = new Scanner(myfile);
       
        PriorityQueue<String> myQ = new PriorityQueue<String>();
       
        while(scanner.hasNextLine()) {
          String s = scanner.nextLine();
          myQ.enqueue(s);
         
        }
      
        
        System.out.println("Output");
       
         while(myQ.size()>0){ 
     
          String x = myQ.dequeue();
          System.out.println(x);
        }
    }
      catch (FileNotFoundException ex){
         System.out.print("ERROR: no file found");
      }
      
}
    
}
